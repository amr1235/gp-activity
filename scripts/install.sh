#!/bin/bash
function python_deps () {
    echo "Creating Virtual Environment..."
    python3 -m venv .venv
    . .venv/bin/activate
    echo "Installing Dependencies..."
    pip install -r requirements.txt
}
function r_deps () {
    echo "install.packages(\"pacman\", repos=\"http://cran.us.r-project.org\")" | R --no-save
}
function install_deps () {
    python_deps
    r_deps
}
install_deps