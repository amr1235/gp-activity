import json
from Bio.Seq import MutableSeq, Seq
import pandas as pd

class Chromosome:
    def __init__(self,sequence, annotations):
        self.sequence = sequence
        self.annotations = annotations
        self.genes = self.genes_masses(self.sequence)
    
    def compare_masses(self, mutations):
        mutated_genes = self.genes_masses(mutations)
        df = pd.DataFrame()
        df["gene_name"] = self.genes["gene_name"]
        df["orig_mass"] = self.genes["mass"].round(3)
        df["mut_mass"] = mutated_genes["mass"].round(3)
        df["delta_mass"] = (df["orig_mass"] - df["mut_mass"]).round(3).abs()
        df = df.sort_values(['orig_mass'], ascending=False)
        return df

    def mutate(self,mutations):
        # Copy the sequence
        cpy_sequence = MutableSeq(self.sequence)
        for i in range(len(mutations["POS"])):
              cpy_sequence[int(mutations["POS"].values[i])] = mutations["ALT_1"].values[i]
        return cpy_sequence.toseq()

    def genes_masses(self,sequence):
        cur_gene = self.annotations["gene_name"].values[0]
        seq = ""
        genes_list = []
        for i in range(len(self.annotations)):
            direction = self.annotations["strand"].values[i]
            start = self.annotations["start"].values[i]
            end = self.annotations["end"].values[i]
            seq += self.get_sequence(sequence, direction, start, end)
            if len(self.annotations) - 1 == i:
                break
            if self.annotations["gene_name"].values[i+1] != cur_gene:    
                mass = self.gene_mass(self.encode_protein(seq))
                seq = ""
                genes_list.append([cur_gene,mass])
                cur_gene = self.annotations["gene_name"].values[i+1]

        mass = self.gene_mass(self.encode_protein(seq))
        genes_list.append([cur_gene,mass])
        head = ["gene_name", "mass"]
        return pd.DataFrame(genes_list,columns=head)

    def get_sequence(self, sequence, direction, start, end):
        if direction == "-":
            return str(sequence[start-1:end].reverse_complement())
        else:
            return str(sequence[start-1:end])

    def encode_protein(self,sequence):
        return Seq(sequence.upper()).translate(to_stop=True)
    
    def gene_mass(self,protein) :
        # read masses file (json)
        file = open("data/masses.json")
        masses = json.load(file)
        return sum([float(masses[p]) for p in protein])